#!/usr/bin/env python3
import unittest
import os
from log import Log


class TestLogging(unittest.TestCase):

	def test_logging(self):
		log = Log("test", "log")
		log.write_message("testing_logging")
		log.write_message("other_log")

		fd = open(log.filename)
		contents = fd.read()
		self.assertRegex(contents, "testing_logging")
		self.assertRegex(contents, "other_log")
		fd.close()

		os.remove(log.filename)
