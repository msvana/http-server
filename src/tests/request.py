#!/usr/bin/env python3
import unittest
from request import extract_path
from request import make_request_from_text
from request import InvalidMethod
from request import InvalidHttpVersion

class TestRequestProcessing(unittest.TestCase):

	def test_correct(self):
		request_text = b"GET /some_location/ HTTP/1.1"
		request = make_request_from_text(request_text)
		self.assertEqual(request["method"], "GET")
		self.assertEqual(request["path"], "/some_location/")
		self.assertEqual(request["http_version"], "HTTP/1.1")

	def test_wrong_http_version(self):
		request_text = b"GET /some/location.txt HTTP"
		with self.assertRaises(InvalidHttpVersion):
			make_request_from_text(request_text)

	def test_wrong_format(self):
		request_text = b"GET hue hue hue HTTP something what does not belong here"
		with self.assertRaises(IndexError):
			make_request_from_text(request_text)

	def test_wrong_method(self):
		request_text = b"POST /some/location/file.txt HTTP/1.1"
		with self.assertRaises(InvalidMethod):
			make_request_from_text(request_text)

	def test_extract_path(self):
		test_uri = "/some%20thi%20ng/#something?name=12"
		path = extract_path(test_uri)
		self.assertEqual("/some thi ng/", path)
