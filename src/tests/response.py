#!/usr/bin/env python3
import unittest
from response import Response


class TestResponseMaking(unittest.TestCase):

	def test_200_no_chunked(self):
		response = Response(200, "hello", 100)
		generated_body = response.get_body()
		generated_headers = response.get_headers()
		self.assertRegex(generated_headers, b'200 OK')
		self.assertRegex(generated_headers, b'xsvana01')
		self.assertRegex(generated_body, b'hello')

	def test_200_chunked(self):
		response = Response(200, "helloworldo", 2)
		self.assertEqual(response.get_next_chunk(), b'he\r\n')
		self.assertEqual(response.get_next_chunk(), b'll\r\n')
		self.assertEqual(response.get_next_chunk(), b'ow\r\n')
		self.assertEqual(response.get_next_chunk(), b'or\r\n')
		self.assertEqual(response.get_next_chunk(), b'ld\r\n')
		self.assertEqual(response.get_next_chunk(), b'o\r\n')
		self.assertEqual(response.get_next_chunk(), b'\r\n')

		self.assertEqual(response.get_next_chunk_size(), b'2\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'2\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'2\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'2\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'2\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'1\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'0\r\n')

	def test_404_chunked(self):
		response = Response(404, "not_found", 3)
		headers = response.get_headers()
		self.assertRegex(headers, b'404 Not Found')

		self.assertEqual(response.get_next_chunk(), b'not\r\n')
		self.assertEqual(response.get_next_chunk(), b'_fo\r\n')
		self.assertEqual(response.get_next_chunk(), b'und\r\n')

	def test_200_utf(self):
		response = Response(404, "Ahoj Šašo", 3)
		self.assertEqual(response.get_next_chunk(), b'Aho\r\n')
		self.assertEqual(response.get_next_chunk(), b'j \xc5\r\n')
		self.assertEqual(response.get_next_chunk(), b'\xa0a\xc5\r\n')
		self.assertEqual(response.get_next_chunk(), b'\xa1o\r\n')
		self.assertEqual(response.get_next_chunk(), b'\r\n')

		self.assertEqual(response.get_next_chunk_size(), b'3\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'3\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'3\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'2\r\n')
		self.assertEqual(response.get_next_chunk_size(), b'0\r\n')
