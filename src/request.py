#!/usr/bin/env python3
import re
from urllib.parse import urlparse


class InvalidMethod(Exception):
	"""
	Thrown when request method is not GET.

	Other methods are not supported and I also want to avoid other random stuff
	client can pass in. 
	"""


class InvalidHttpVersion(Exception):
	"""
	Thrown when request HTTP version is not set to HTTP/1.1
	This also helps checking if the request has correct form
	"""


def make_request_from_text(request_text):
	""" 
	Takes possible http request in text form and creates it's inner
	representation.

	Function is interested only in the first line of the request: method, uri
	and http version.

	Param request_text: must be a bytes object
	Returns: dict with method, uri, path and http_version keys

	Throws: UnknownMethod, InvalidHttpVersion, IndexError
	"""
	request_text = request_text.decode("utf-8")
	first_line = request_text.splitlines()[0]
	request_data = first_line.split(" ")

	if len(request_data) != 3:
		message = "Invalid request format"
		raise IndexError(message)

	request = dict(
		method=request_data[0],
		uri=request_data[1],
		http_version=request_data[2])

	validate_request(request)
	request["path"] = extract_path(request["uri"])
	return request


def extract_path(uri):
	"""
	Extracts path from given URI string and decodes special chars encoded using
	pecent notation
	"""
	parsed_uri = urlparse(uri)
	path = re.sub("\%([0-9A-Fa-f]{2})", decode_percent_char, parsed_uri.path)
	path = path if path[0] == "/" else "/%s" % path
	return path


def decode_percent_char(match):
	"""
	Replaces percent representation of a character in the GET request
	by the character it actually represents

	Is passed to re.sub() function as repl argument.

	Param match: re.MatchObject, match.group(1) should be hex digits
	Returns: character represented by hex digits
	"""
	hex_number = "0x%s" % match.group(1)
	char_number = int(hex_number, 16)
	char = chr(char_number)
	return char


def validate_request(request):
	"""
	Checks if the request dict extracted by make_request_from_text() contains
	valid HTTP request

	My server supports only GET method and HTTP/1.1 version of protocol. 
	Nothing else is allowed. Uri is tested only on character set and restricts
	uri to absolute path only

	Returns: NoneType

	Throws: InvalidHttpVersion, UnknownMethod
	"""
	if request["http_version"] != "HTTP/1.1":
		http_version = request["http_version"]
		message = "Server supports only HTTP 1.1 protocol %s given" % http_version
		raise InvalidHttpVersion(message)
		
	if request["method"] != "GET":
		method = request["method"]
		message = "Server supports only GET requests %s given" % method
		raise InvalidMethod(message)
