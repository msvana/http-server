#!/usr/bin/env python3
import config
import socket
from request import make_request_from_text
from request import InvalidMethod
from request import InvalidHttpVersion
from responsebody import make_response_body
from responsebody import DoesNotExist
from responsebody import Forbidden
from response import Response
from log import Log
from random import randint
from time import sleep

def start_server(port, chunk_size, min_chunk_delay):
	"""
	Starts the http server

	Param port: server will be serving on this port number
	Param chunk_size: Chunk size for spliting responses, 0 to turn of
	Param min_chunk_delay: Minimal delay in miliseconds between sending two 
	  chunks. Maximum is 2*min_chunk_delay

	Returns: None
	"""
	try:
		listen_socket = init_socket(port)
		listen_socket.listen(1)
		request_loop(listen_socket, chunk_size, min_chunk_delay)
	except PermissionError:
		print("This port is propably already in use")
	except OverflowError:
		print("Port number must be between 0 and 65535")


def init_socket(port):
	"""
	Creates new socket for listening to requests and binds it to the given port

	Param port: port number which will be the socket bound to
	Returns: Socket
	"""
	listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	listen_socket.bind(("", port))
	return listen_socket


def request_loop(listen_socket, chunk_size, min_chunk_delay):
	"""
	Waits for client requests and responds to them

	Param listen_socket: socket for accepting client requests
	Param chunk_size: size of chunks which will be the response split into
	Param min_chunk_delay: Minimal delay in miliseconds between sending two 
	  chunks. Maximum is 2*min_chunk_delay

	Returns: None
	"""
	in_log = Log(config.SERVER_LOG_BASE, config.LOG_SUFFIX_IN)
	out_log = Log(config.SERVER_LOG_BASE, config.LOG_SUFFIX_OUT)

	while True:
		try:
			client_connection, client_address = listen_socket.accept()
			request_text = client_connection.recv(config.REQUEST_SIZE_LIMIT)

			if len(request_text) == 0:
				continue

			in_log.write_message(request_text)
			response = build_response(request_text, chunk_size)
			out_log.write_message(response.get_headers())
			client_connection.sendall(response.get_headers(), socket.MSG_WAITALL)
			sleep(0.01)

			if response.is_using_chunks():
				send_all_chunks(client_connection, response, out_log, min_chunk_delay)
			else:
				out_log.write_message(response.get_body())
				client_connection.sendall(response.get_body())

			client_connection.close()

		except KeyboardInterrupt:
			print("\nQuitting the server ...")
			try: client_connection.close()
			except: pass
			return


def build_response(request_text, chunk_size):
	"""
	Builds http response object. Takes care of all exceptions which can be
	thrown during this process.

	Param request_text: raw http request text
	Param chunk_size: size of chunks which will be the response split into
	Param min_chunk_delay: Minimal delay in miliseconds between sending two 
	  chunks. Maximum is 2*min_chunk_delay

	Returns: Response
	"""
	try:
		request = make_request_from_text(request_text)
		response_body = make_response_body(request)
		response = Response(200, response_body, chunk_size)
	except DoesNotExist:
		response_body = config.RESPONSE_NOT_FOUND
		response = Response(404, response_body, chunk_size)
	except Forbidden:
	 	response_body = config.RESPONSE_FORBIDDEN
	 	response = Response(403, response_body, chunk_size)
	except (InvalidMethod, InvalidHttpVersion):
		response_body = config.RESPONSE_FORBIDDEN
		response = Response(501, response_body, chunk_size)
	return response


def send_all_chunks(connection, response, out_log, min_chunk_delay):
	"""
	When using chunked transfer encoding this function will send all chunks
	to the client

	Param connection: socket to send the chunks to
	Param response: Response object which will be sent to client
	Param out_log: Log object for logging outcomming communication
	Param min_chunk_delay: Minimal delay in miliseconds between sending two 
	  chunks. Maximum is 2*min_chunk_delay

	Returns None
	"""
	while True:
		chunk_delay = randint(min_chunk_delay, 2*min_chunk_delay)
		sleep(float(chunk_delay) / 1000)
		try:
			chunk = response.get_next_chunk()
			chunk_size = response.get_next_chunk_size()
		except IndexError:
			return
		out_log.write_message(chunk_size)
		out_log.write_message(chunk)
		connection.sendall(chunk_size)
		connection.sendall(chunk)
