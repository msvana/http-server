#!/usr/bin/env python3

"""
Contains http server configuration options which are not set
using command line arguments
"""

BASE_PATH = "./WWW"
REQUEST_SIZE_LIMIT = 2048

STATUS_TEXTS = {
	200: "OK",
	403: "Forbidden",
	404: "Not Found",
	501: "Not Implemented" 
}

RESPONSE_NOT_FOUND = "ERROR 404: REQUESTED FILE OR DIR NOT FOUND"
RESPONSE_NOT_IMPLEMENTED = "ERROR 501: PLEASE USE GET METHOD"
RESPONSE_FORBIDDEN = "ERROR 403: FORBIDDEN ACCESS"

SERVER_LOG_BASE = "ipkHttpServer"
CLIENT_LOG_BASE = "ipkHttpClient"

LOG_SUFFIX_IN = ".in.log"
LOG_SUFFIX_OUT = ".out.log"
